import java.util.ArrayList;

/**
 * This library which includes different methods to work with vectors (computtational geometry)
 */
public class MathAndComputtationalGeometry {
    /**
     * Method calculates oblique product of vectors (p1, p2) and (p1, p3)
     * @param p1
     * @param p2
     * @param p3
     * @return
     */
    public static double oblProdOfVec(Point p1, Point p2, Point p3) {
        return (p2.getX() - p1.getX()) * (p3.getY() - p1.getY()) - (p2.getY() - p1.getY()) * (p3.getX() - p1.getX());
    }


    /**
     * Method calculates scalar product of vectors (p1, p2) and (p1, p3)
     * @param p1
     * @param p2
     * @param p3
     * @return
     */
    public static double scalProdOfVec(Point p1, Point p2, Point p3) {
        return (p2.getX() - p1.getX()) * (p3.getX() - p1.getX()) + (p2.getY() - p1.getY()) * (p3.getY() - p1.getY());
    }


    /**
     * Method determines: Does point belong to segment or not?
     * @param line
     * @param point
     * @return
     */
    public static boolean belPointToSegm(Line line, Point point) {
        return (oblProdOfVec(line.getP1(), line.getP2(), point) == 0) && (scalProdOfVec(point, line.getP1(), line.getP2()) <= 0);
    }


    /**
     * Methode determines: Does lines intersect or not?
     * @param l1
     * @param l2
     * @return
     */
    public static boolean intersectionLines(Line l1, Line l2) {
        /*
        It is special case when segmenets lie on one line, but they have one or greater common points.
        We need this special checking because when our segments lie on one line and have one or greater common points our method will return false
        because oblique product will equal zero
        */
        if (belPointToSegm(l1, l2.getP1()) || belPointToSegm(l1, l2.getP2()) || belPointToSegm(l2, l1.getP1()) || belPointToSegm(l2, l1.getP2())) {
            return true;
        // It is default checking using oblique product of vectors
        } else if (((oblProdOfVec(l1.getP1(), l1.getP2(), l2.getP1()) * oblProdOfVec(l1.getP1(), l1.getP2(), l2.getP2()) < 0) &&
                (oblProdOfVec(l2.getP1(), l2.getP2(), l1.getP1()) * oblProdOfVec(l2.getP1(), l2.getP2(), l1.getP2()) < 0))) {
            return true;
        }
        return false;
    }


    /**
     * Method returns max/min X coordinate from inputing ArrayList of points
     * @param list
     * @return
     */
    public static double getMaxMinCoordinateX(ArrayList<Point> list, String mod) {
        double max = list.get(0).getX(), min = list.get(0).getX();
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).getX() > max) max = list.get(i).getX();
            if (list.get(i).getX() < min) min = list.get(i).getX();
        }
        if (mod.toLowerCase().equals("max")) return max;
        if (mod.toLowerCase().equals("min")) return min;
        return Double.parseDouble(null); // If "mod" doesn`t equal to "min"/"max'
    }


    /**
     * Method return max/min Y coordinate from inputing ArrayList of points
     * @param list
     * @param mod
     * @return
     */
    public static double getMaxMinCoordinateY(ArrayList<Point> list, String mod) {
        double max = list.get(0).getY(), min = list.get(0).getY();
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).getY() > max) max = list.get(i).getY();
            if (list.get(i).getY() < min) min = list.get(i).getY();
        }
        if (mod.toLowerCase().equals("max")) return max;
        if (mod.toLowerCase().equals("min")) return min;
        return Double.parseDouble(null); // If "mod" doesn`t equal to "min"/"max'
    }
}
