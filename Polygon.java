import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

/**
 * This is class of implementation of polygon
 */
public class Polygon {
    // Field of class
    private ArrayList<Point> points_of_polygon; // ArrayList of point of polygon
    private double min_x, min_y, max_x, max_y; // There are coordinates of sqaure which limit polygon
    private Random random = new Random(); // We need more Random!!!!!

    /**
     * Constructor of class "Polygon" where initialization or main field - "point_of_polygon"
     * Also method finds max/min X/Y
     * @param points
     */
    Polygon(ArrayList<Point> points) {
        points_of_polygon = new ArrayList<Point>(points);
        max_x = MathAndComputtationalGeometry.getMaxMinCoordinateX(points_of_polygon, "max");
        min_x = MathAndComputtationalGeometry.getMaxMinCoordinateX(points_of_polygon, "min");
        max_y = MathAndComputtationalGeometry.getMaxMinCoordinateY(points_of_polygon, "max");
        min_y = MathAndComputtationalGeometry.getMaxMinCoordinateY(points_of_polygon, "min");
    }

    /**
     * Method counts of intersection edges of our polygon with line
     * @param line
     * @return
     */
    private int getAmountOfIntersection(Line line) {
        int counter = 0;
        for (int i = 0; i < points_of_polygon.size(); i++) {
            if (i == points_of_polygon.size() - 1) {
                // It`s special case when we connect last one and first point
                if (MathAndComputtationalGeometry.intersectionLines(line, new Line(points_of_polygon.get(i).getX(), points_of_polygon.get(i).getY(),
                        points_of_polygon.get(0).getX(), points_of_polygon.get(0).getY()))) {
                    counter++;
                }
            } else if (MathAndComputtationalGeometry.intersectionLines(line, new Line(points_of_polygon.get(i).getX(), points_of_polygon.get(i).getY(),
                    points_of_polygon.get(i + 1).getX(), points_of_polygon.get(i + 1).getY()))) {
                counter++;
            }
        }
        return counter;
    }


    /**
     * Method determines: Does point belong to edges or summit of polygon or not
     * We need it when we put point in summit or in edge and even-odd rule doesn`t work
     * @param point
     * @return
     */
    private boolean checkPointOnContourBelonging(Point point) {
        for (int i = 0; i < points_of_polygon.size(); i++) {
            if (i == points_of_polygon.size() - 1) {
                if (MathAndComputtationalGeometry.belPointToSegm(new Line(points_of_polygon.get(i).getX(), points_of_polygon.get(i).getY(),
                        points_of_polygon.get(0).getX(), points_of_polygon.get(0).getY()), point)) {
                    return true;
                }
            } else if (MathAndComputtationalGeometry.belPointToSegm(new Line(points_of_polygon.get(i).getX(), points_of_polygon.get(i).getY(),
                    points_of_polygon.get(i + 1).getX(), points_of_polygon.get(i + 1).getY()), point)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Method detemrmies: Does point belong to polygon or not?
     * @param point
     * @return
     */
    public boolean checkPointOnPolygonBelonging(Point point) {
        Point2D point_outside_square = new Point2D.Double((random.nextDouble() + max_x), (random.nextDouble() + max_y)); // Here is random outside sqaure point

        // It is special case when point lies on summit or edge ot polygon
        if (checkPointOnContourBelonging(point)) {
            return true;
        // Counting of intersection
        } else if (getAmountOfIntersection(new Line(point.getX(), point.getY(),
                point_outside_square.getX(), point_outside_square.getY())) % 2 != 0) {
            return true;
        }
        return false;
    }


    /**
     * Method calculate square of polygon using Monte-Carlo simulation with accuracy equal "epsilon"
     * @param epsilon
     * @return
     */
    public double getSquare(double epsilon) {
        // Declaration
        double sqaure = 0, previous_square = 0; // For calculation of delta square
        long counter_of_inside_points = 0; // Amount of points inside polygon
        int total_points_counter = 0; // Total amount of points

        // Main loop
        // "total_points_counter <= 1000" condition is for start our loop normally
        while (total_points_counter <= 10000 || Math.abs(sqaure - previous_square) >= epsilon) {
            // Random outside of square point
            Point point_inside_square = new Point((random.nextDouble() * (max_x - min_x) + min_x), (random.nextDouble() * (max_y - min_y) + min_y));

            if (checkPointOnPolygonBelonging(point_inside_square)) counter_of_inside_points++; // increment of inside point counter

            total_points_counter++; // increment of total counter

            // Delta square
            previous_square = sqaure;
            sqaure = (max_x - min_x) * (max_y - min_y) * ((double)counter_of_inside_points/total_points_counter);
        }
        return sqaure;
    }
}
